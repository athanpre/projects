USE [PhilNBI]
GO

/****** Object:  Table [dbo].[UserAccount_Info]    Script Date: 09/26/2015 11:01:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserAccount_Info](
	[UserName] [nvarchar](50) NOT NULL,
	[UserPassword] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[MiddleName] [nvarchar](100) NOT NULL,
	[GivenName] [nvarchar](100) NOT NULL,
	[EmailAddress] [nvarchar](200) NULL,
	[Department] [nvarchar](100) NOT NULL,
	[IsAdministrator] [bit] NOT NULL,
	[IsNormalUser] [bit] NOT NULL,
	[IsDepartment1] [bit] NOT NULL,
	[IsDepartment2] [bit] NOT NULL,
	[IsDepartment3] [bit] NOT NULL,
	[IsDepartment4] [bit] NOT NULL,
	[IsDepartment5] [bit] NOT NULL,
	[TimeAdded] [datetime] NOT NULL,
	[Remarks] [nvarchar](200) NULL,
 CONSTRAINT [pk_indexid] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


